
		for x in range(0,256):
			c = Sbox[(x/2)] & 0xffffL
			s1 =  c >>> 8 if   ((x & 1) == 0) ?  else c & 0xff
			s2 = s1 << 1
			if s2 >= 0x100L:
				s2 ^= 0x11dL
			
			s3 = s2 ^ s1
			s4 = s2 << 1

			if s4 >= 0x100L:
				s4 ^= 0x11dL
			
			s5 = s4 ^ s1
			s6 = s4 ^ s2
			s7 = s6 ^ s1
			s8 = s4 << 1
			if s8 >= 0x100L:
				s8 ^= 0x11dL
			
			sb = s8 ^ s2 ^s1

			T[0][x] = (s1 << 56) | (s3 << 48) | (s4 << 40) | (s5 << 32) | (s6 << 24) | (s8 << 16) | (sb << 8) | s7
			T[1][x] = (s3 << 56) | (s1 << 48) | (s5 << 40) | (s4 << 32) | (s8 << 24) | (s6 << 16) | (s7 << 8) | sb
			T[2][x] = (s4 << 56) | (s5 << 48) | (s1 << 40) | (s3 << 32) | (sb << 24) | (s7 << 16) | (s6 << 8) | s8
			T[3][x] = (s5 << 56) | (s4 << 48) | (s3 << 40) | (s1 << 32) | (s7 << 24) | (sb << 16) | (s8 << 8) | s6
			T[4][x] = (s6 << 56) | (s8 << 48) | (sb << 40) | (s7 << 32) | (s1 << 24) | (s3 << 16) | (s4 << 8) | s5
			T[5][x] = (s8 << 56) | (s6 << 48) | (s7 << 40) | (sb << 32) | (s3 << 24) | (s1 << 16) | (s5 << 8) | s4
			T[6][x] = (sb << 56) | (s7 << 48) | (s6 << 40) | (s8 << 32) | (s4 << 24) | (s5 << 16) | (s1 << 8) | s3
			T[7][x] = (s7 << 56) | (sb << 48) | (s8 << 40) | (s6 << 32) | (s5 << 24) | (s4 << 16) | (s3 << 8) | s1
			S[x] = s1
		
        for r in range(0,R):
			c[r] =
				((Sbox[(4*r + 0)] & 0xffffL) << 48) | 
				((Sbox[(4*r + 1)] & 0xffffL) << 32) | 
				((Sbox[(4*r + 2)] & 0xffffL) << 16) | 
				((Sbox[(4*r + 3)] & 0xffffL)      )

	roundKeyEnc = [R + 1]
	roundKeyDec = [R + 1]

    public Khazad() {}

	"""
	 * Create the Khazad key schedule for a given cipher key.
	 * 
     * @param key   The 128-bit cipher key.
	"""
	public final void keySetup(byte[/*16*/] key) {
		
		if len(lenkey) != 16:
			print("Invalid Khazad key size: " + (8*len(key)) + " bits.")
            return

		K2 =
			((key[ 0]       ) << 56) ^
			((key[ 1] & 0xff) << 48) ^
			((key[ 2] & 0xff) << 40) ^
			((key[ 3] & 0xff) << 32) ^
			((key[ 4] & 0xff) << 24) ^
			((key[ 5] & 0xff) << 16) ^
			((key[ 6] & 0xff) <<  8) ^
			((key[ 7] & 0xff)      )
		K1 =
			((key[ 8]       ) << 56) ^
			((key[ 9] & 0xff) << 48) ^
			((key[10] & 0xff) << 40) ^
			((key[11] & 0xff) << 32) ^
			((key[12] & 0xff) << 24) ^
			((key[13] & 0xff) << 16) ^
			((key[14] & 0xff) <<  8) ^
			((key[15] & 0xff)      )

		"""
		 * compute the round keys:
		"""
        for r in range(0,R):
		
			"""
			  K[r] = rho(c[r], K1) ^ K2
			"""
			roundKeyEnc[r] =
				T[0][(K1 >>> 56)       ] ^
				T[1][(K1 >>> 48) & 0xff] ^
				T[2][(K1 >>> 40) & 0xff] ^
				T[3][(K1 >>> 32) & 0xff] ^
				T[4][(K1 >>> 24) & 0xff] ^
				T[5][(K1 >>> 16) & 0xff] ^
				T[6][(K1 >>>  8) & 0xff] ^
				T[7][(K1       ) & 0xff] ^
				c[r] ^ K2
			K2 = K1
            K1 = roundKeyEnc[r]
		
		"""
		 * compute the inverse key schedule:
		 * K'^0 = K^R, K'^R = K^0, K'^r = theta(K^{R-r})
		"""
		roundKeyDec[0] = roundKeyEnc[R]
        for r in range(1,R):
			K1 = roundKeyEnc[R - r]
			roundKeyDec[r] =
				T[0][S[(K1 >>> 56)       ]] ^
				T[1][S[(K1 >>> 48) & 0xff]] ^
				T[2][S[(K1 >>> 40) & 0xff]] ^
				T[3][S[(K1 >>> 32) & 0xff]] ^
				T[4][S[(K1 >>> 24) & 0xff]] ^
				T[5][S[(K1 >>> 16) & 0xff]] ^
				T[6][S[(K1 >>>  8) & 0xff]] ^
				T[7][S[(K1       ) & 0xff]]
		
		roundKeyDec[R] = roundKeyEnc[0]
	} # keySetup

	"""
     * Either encrypt | decrypt a data block, according to the key schedule.
     * 
     * @param	block		the data block to be encrypted/decrypted.
     * @param	roundKey	the key schedule to be used.
    """
    protected final void crypt(byte[/*8*/] block, long[/*R + 1*/] roundKey) {
        """
		 * map byte array block to cipher state (mu)
		 * & add initial round key (sigma[K^0]):
		"""
		state =
			((block[0]       ) << 56) ^
			((block[1] & 0xff) << 48) ^
			((block[2] & 0xff) << 40) ^
			((block[3] & 0xff) << 32) ^
			((block[4] & 0xff) << 24) ^
			((block[5] & 0xff) << 16) ^
			((block[6] & 0xff) <<  8) ^
			((block[7] & 0xff)      ) ^
			roundKey[0]

        """
		 * R - 1 full rounds:
		"""

        for r in range(1,R):
			state =
				T[0][(state >>> 56)       ] ^
				T[1][(state >>> 48) & 0xff] ^
				T[2][(state >>> 40) & 0xff] ^
				T[3][(state >>> 32) & 0xff] ^
				T[4][(state >>> 24) & 0xff] ^
				T[5][(state >>> 16) & 0xff] ^
				T[6][(state >>>  8) & 0xff] ^
				T[7][(state       ) & 0xff] ^
				roundKey[r]
        

        """
		 * last round:
		"""
		state =
			(T[0][(state >>> 56)         ] & 0xff00000000000000L) ^
			(T[1][(state >>> 48) & 0xff] & 0x00ff000000000000L) ^
			(T[2][(state >>> 40) & 0xff] & 0x0000ff0000000000L) ^
			(T[3][(state >>> 32) & 0xff] & 0x000000ff00000000L) ^
			(T[4][(state >>> 24) & 0xff] & 0x00000000ff000000L) ^
			(T[5][(state >>> 16) & 0xff] & 0x0000000000ff0000L) ^
			(T[6][(state >>>  8) & 0xff] & 0x000000000000ff00L) ^
			(T[7][(state       ) & 0xff] & 0x00000000000000ffL) ^
			roundKey[R]

		"""
		 * map cipher state to byte array block (mu^{-1}):
		"""
		block[0] = (byte)(state >>> 56)
		block[1] = (byte)(state >>> 48)
		block[2] = (byte)(state >>> 40)
		block[3] = (byte)(state >>> 32)
		block[4] = (byte)(state >>> 24)
		block[5] = (byte)(state >>> 16)
		block[6] = (byte)(state >>>  8)
		block[7] = (byte)(state       )
    }

	"""
     * Encrypt a data block.
     * 
     * @param	block	the data buffer to be encrypted.
    """
    public final void encrypt(byte[/*8*/] block) {
		crypt(block, roundKeyEnc)
    }

	"""
     * Decrypt a data block.
     * 
     * @param	block	the data buffer to be decrypted.
    """
    public final void decrypt(byte[/*8*/] block) {
		crypt(block, roundKeyDec)
    }

	private static String display(byte[] array) {
		val = []
		hex = "0123456789ABCDEF"

        for r in range(0,len(array)):
			int b = array[i] & 0xff
			val[2*i] = hex[(b >>> 4)]
			val[2*i + 1] = hex[(b & 15)]
		
		return chr(val)
	}

	"""
	 * Generate the test vector set for Khazad.
	 *
	 * The test consists of the encryption of every block
	 * with a single bit set under every key with a single
	 * bit set for every allowed key size.
	"""
	public static void makeTestVectors() {
		print("Khazad test vectors")
		print("--------------------------------------------------")
        Khazad a = new Khazad()

		byte[] key = new byte[16]
        
        for r in range(0,len(key)):
			key[i] = 0
		
		byte[] block = new byte[8]

        for r in range(0,len(block)):
			block[i] = 0
		
		"""
		elapsed = -System.currentTimeMillis()
		for (int i = 0 i < 100000 i++) {
			a.keySetup(key)
		}
		elapsed += System.currentTimeMillis()
		print("Key setup time: " + elapsed + " ms.")
		if (key[0] == 0) {
			return
		}
		"""

		"""
		 * iteration test -- encrypt the null block under the null key a billion times:
		"""
		print("iteration test:")
		System.out.print("\tKEY: " + display(key))
		a.keySetup(key)

        for k in range(0,1000):
			a.encrypt(block)

		print("\tCT: " + display(block))
        
        for k in range(0,1000):
			a.decrypt(block)


        for i in range(0,len(block)):
			if block[i] != 0:
				print("ERROR IN ITERATION TEST!")
				return

		print("Null block under all keys with a single bit set:")
        for k in range(k,128):
			# set the k-th bit:
			key[k/8] |= (byte)(0x80 >>> (k%8))
			print("\tKEY: " + display(key))
			# setup key:
			a.keySetup(key)
			# encrypt the null block:
			a.encrypt(block)
			print("\tCT: " + display(block))
			# decrypt:
			a.decrypt(block)
            for i in range(0,len(block)):
				if block[i] != 0:
					print("ERROR IN SINGLE-BIT KEY TEST!")
					return
			# reset the k-th key bit:
			key[k/8] = 0
		print("--------------------------------------------------")
	}

	public static void main(String[] args) throws java.io.IOException {
        Khazad.makeTestVectors()
		print("Press <ENTER> to finish...")
		System.in.read()
	}

}