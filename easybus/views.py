from math import ceil
from decimal import Decimal
import json
from django.shortcuts import  render_to_response, render
from django.core import serializers
from easybus.models import Trajeto, Itinerario, Rota, Cidade, Bairro, CidadeBairroTrajeto, MapeamentoOnibus
import datetime
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect

def home(request):
    return render_to_response('easybus/base/base.html', {})

def teste(request, valor):
    print('cheguei?')
    print(valor)
    retorno = []
    retorno.append('carai')
    return HttpResponse(json.dumps(retorno), content_type='application/json')

###################################################################################################
# ------------------------------- MANUTENCAO DE TRAJETO DE ONIBUS ------------------------------- #
###################################################################################################
def listTrajeto(request):
    listaLabels = ["Código", "Descrição"]
    result = {}
    result['listaItens'] = Trajeto.objects.all().order_by('id')
    result['listaLabels'] = listaLabels
    result['pageTitle'] = 'Manutenção dos Trajetos de Ônibus'
    result['pageIcon'] = 'fa-road'
    result['title'] = 'Trajetos de Ônibus'
    result['manutUrl'] = "/trajeto/"
    result['searchUrl'] = "buscaTrajetos"

    result = paginationProperties(result)
    return render(request, 'easybus/trajeto/listTrajeto.html', result)

def manutTrajeto(request, crudOption, ID=0):

    context = getCRUDProperties(crudOption) 
    context['title'] = 'Trajeto de Ônibus'
    context['crudListUrl'] = '/trajeto/'
    context['pageIcon'] = 'fa-shopping-cart'
    context['listaLabels'] = ["Sequência", "Parada", "Endereço",
                              "Bairro/Localidade", "Cidade", "Tempo", "Adicional", "Ações"]
    context['crudOption'] = crudOption

    trajeto = Trajeto()
    itinerario = []

    if crudOption != 'ADD':
        trajeto = Trajeto.objects.get(id=ID)
        itinerario = serializers.serialize("json", trajeto.getItinerario())

    if request.method != 'POST':
        context['trajeto'] = trajeto
        context['listaItinerario'] = itinerario
        return render(request, 'easybus/trajeto/manutTrajeto.html', context)

    return syncTrajeto(request, crudOption, ID, trajeto)    

def syncTrajeto(request, crudOption, ID, trajeto):
    if crudOption == 'RMV':
        return deleteObjeto(trajeto, 'Trajeto')
    return saveTrajeto(request, crudOption, trajeto)

def saveTrajeto(request, crudOption, trajeto):
    messages = []
    success = False
    itinerarioSelecionado = request.POST.getlist('itinerario')

    if not itinerarioSelecionado:
        messages.append('Ao menos um Itinerario deve ser cadastrado')
        return myReturnJsonResponse(messages, success)

    trajeto.descricao = request.POST['descricao']
    trajeto.valor = request.POST['valorPassagem'].replace(',','.')
    trajeto.save()

    for itinerario in Itinerario.objects.all().filter(trajeto=trajeto.id):
        itinerario.delete()

    for item in itinerarioSelecionado:
        item = json.loads(item)
        itinerario = Itinerario()
        itinerario.bairro = item['bairro']
        itinerario.cidade = item['cidade']
        itinerario.latitude = item['latitude']
        itinerario.longitude = item['longitude']
        itinerario.local = item['local']
        itinerario.parada = item['parada']
        itinerario.sequencia = item['sequencia']
        itinerario.tempo = item['tempo']
        itinerario.tempoAdicional = retornaTempoAdicional(item['tempoAdicional'])
        itinerario.trajeto = trajeto
        itinerario.save()
        cidade = syncCidade(itinerario.cidade)
        bairro = syncBairro(cidade, itinerario.bairro)
        syncCidadeBairroTrajeto(cidade, bairro, trajeto)

    if crudOption == 'MOD':
        calcChegadaRota(trajeto)

    success = True
    messages.append('Trajeto '
                    + ('adicionada' if crudOption == 'ADD' else 'modificada')
                    + ' com sucesso!')

    return myReturnJsonResponse(messages, success)

def retornaTempoAdicional(tempo):
    print('chegou aqui')
    '''if ',' in tempo or '.' in tempo:
        print('com virgula: ' + tempo)'''
    return tempo
    '''else:
        print('sem virgula: ' + tempo)
        (h, m) = tempo.split(':')
        return int(h) * 3600 + int(m) * 60'''

def getItinerarioByTrajeto(request, *args, **kwargs):
    retorno = {}
    mapQuery = []

    if request.method == 'POST':
        retorno['success'] = True
        mapQuery = json.loads(request.POST['client_response'])

        trajeto = Trajeto.objects.get(id=mapQuery["idTrajeto"])
        retorno['itinerario'] = serializers.serialize("json", trajeto.getItinerario())
    else:
        retorno['success'] = False
        retorno['messages'] = 'Erro'

    retorno = json.dumps(retorno)
    return HttpResponse(retorno, content_type='application/json')

###################################################################################################
# ---------------------------------- MANUTENCAO DE ROTAS DE ONIBUS ------------------------------ #
###################################################################################################
def listRota(request):

    listaLabels = ["Código", "Trajeto", "Saida",
                   "Chegada", "Estudante", "Necessidades Especiais",
                   "Feriados"]
    result = {}
    result['listaItens'] = Rota.objects.all().order_by('id')
    result['listaLabels'] = listaLabels
    result['pageTitle'] = 'Manutenção das Rotas de Ônibus'
    result['pageIcon'] = 'fa-map'
    result['title'] = 'Rotas de Ônibus'
    result['manutUrl'] = "/rota/"
    result['searchUrl'] = 'buscaRotas'
    result = paginationProperties(result)

    return render(request, 'easybus/rota/listRota.html', result)

def manutRota(request, crudOption, ID=0):

    context = getCRUDProperties(crudOption)
    context['title'] = 'Rotas de Ônibus'
    context['crudListUrl'] = '/rota/'
    context['trajetos'] = Trajeto.objects.all().order_by('id')
    context['listaLabels'] = ["Sequência", "Parada", "Endereço",
                              "Bairro/Localidade", "Cidade", "Horário"]
    rota = Rota()
    itinerario = serializers.serialize("json", Trajeto.objects.first().getItinerario())

    if crudOption != 'ADD':
        rota = Rota.objects.get(id=ID)
        itinerario = serializers.serialize("json", rota.trajeto.getItinerario())

    if request.method != 'POST':
        context['rota'] = rota
        context['listaItinerario'] = itinerario
        return render(request, 'easybus/rota/manutRota.html', context)

    return syncRota(request, crudOption, ID, rota)

def syncRota(request, crudOption, ID, rota):
    if crudOption == 'RMV':
        return deleteObjeto(rota, 'Rota')
    return saveRota(request, crudOption, rota)

def saveRota(request, crudOption, rota):
    messages = []
    success = False

    rota.trajeto = Trajeto.objects.get(id=request.POST['trajeto'])
    rota.saida = request.POST['saida']
    rota.chegada = request.POST['chegada']
    rota.estudante = returnBooleanValue(request.POST, 'estudante')
    rota.especiais = returnBooleanValue(request.POST, 'especiais')
    rota.domingo = returnBooleanValue(request.POST, 'domingo')
    rota.segunda = returnBooleanValue(request.POST, 'segunda')
    rota.terca = returnBooleanValue(request.POST, 'terca')
    rota.quarta = returnBooleanValue(request.POST, 'quarta')
    rota.quinta = returnBooleanValue(request.POST, 'quinta')
    rota.sexta = returnBooleanValue(request.POST, 'sexta')
    rota.sabado = returnBooleanValue(request.POST, 'sabado')
    rota.feriado = returnBooleanValue(request.POST, 'feriado')

    if rota.trajeto is None:
        messages.append('Trajeto selecionado inválido!')
        return myReturnJsonResponse(messages, success)

    if (not rota.domingo
            and not rota.segunda
            and not rota.terca
            and not rota.quarta
            and not rota.quinta
            and not rota.sexta
            and not rota.sabado):
        messages.append('Ao menos um dia de operação deve ser selecionado!')
        return myReturnJsonResponse(messages, success)

    rota.save()

    success = True
    messages.append('Rota '
                    + ('adicionada' if crudOption == 'ADD' else 'modificada')
                    + ' com sucesso!')
    return myReturnJsonResponse(messages, success)


def getTrajetoByRota(request, *args, **kwargs):

    retorno = {}
    mapQuery = []

    if request.method == 'POST':
        mapQuery = json.loads(request.POST['client_response'])

    rota = Rota.objects.get(id=mapQuery["idRota"])
    retorno['itinerario'] = serializers.serialize("json", rota.trajeto.getItinerario())

    mapeamento = []

    try:
        mapeamento = MapeamentoOnibus.objects.get(rota=rota.id)
    except MapeamentoOnibus.DoesNotExist:
        mapeamento = None

    if mapeamento != None:
        retorno['posicaoOnibus'] = json.dumps({'latitude' : mapeamento.latitude, 'longitude' : mapeamento.longitude})

    retorno = json.dumps(retorno)

    return HttpResponse(retorno, content_type='application/json')

###################################################################################################
# ---------------------------------------- BUSCA DE ONIBUS -------------------------------------- #
###################################################################################################

def buscaOnibus(request):
    listaLabels = ["Rota", "Trajeto", "Saida",
                   "Chegada", "Estudante", "Necessidades Especiais","Dias de Operação",
                   "Feriados","Valor Passagem"]
    result = {}
    result['listaItens'] = []
    result['listaLabels'] = listaLabels
    result['pageTitle'] = 'Busca de Ônibus'
    result['pageIcon'] = 'fa-bus'
    result['title'] = 'Busca de Ônibus'
    result['searchUrl'] = 'buscaOnibus'
    result['cidadeOrigem'] = Cidade.objects.all()
    result['bairroOrigem'] = Bairro.objects.all().filter(cidade=Cidade.objects.first().id)
    result['cidadeDestino'] = Cidade.objects.all()
    result['bairroDestino'] = Bairro.objects.all().filter(cidade=Cidade.objects.first().id)

    result = paginationProperties(result)

    return render(request, 'easybus/buscaOnibus/buscaOnibus.html', result)

def retornaRotasBuscaOnibus(request, *args, **kwargs):
    mapQuery = []
    resultOrigem = []
    resultDestino = []
    resultRotas = []
    retorno = []
    html = []

    if request.method == 'POST':
        mapQuery = json.loads(request.POST['client_response'])

    for origem in CidadeBairroTrajeto.objects.all().filter(cidade=int(mapQuery['cidadeOrigem']),
                                                           bairro=int(mapQuery['bairroOrigem'])):
        resultOrigem.append(origem)

    for destino in CidadeBairroTrajeto.objects.all().filter(cidade=int(mapQuery['cidadeDestino']),
                                                            bairro=int(mapQuery['bairroDestino'])):
        resultDestino.append(destino)

    for origem in resultOrigem:
        for destino in resultDestino:
            if origem.trajeto == destino.trajeto:
                resultRotas.append(Rota.objects.all().filter(trajeto=origem.trajeto))

    for rotas in resultRotas:
        for rota in rotas:
            dias = getDiasOperacao(rota)
            html.append('<tr>' +
                        '<td width="50">' + str(rota.id) + '</td>' +
                        '<td>' + str(rota.trajeto.id) + ' - ' + str(rota.trajeto.descricao) + '</td>' +
                        '<td>' + str(rota.saida) + '</td>' +
                        '<td>' + str(rota.chegada) + '</td>' +
                        '<td>' + ('Sim' if rota.estudante == 1  else 'Não') + '</td>' +
                        '<td>' + ('Sim' if rota.especiais == 1  else 'Não') + '</td>' +
                        '<td>' + dias + '</td>' +
                        '<td>' + ('Sim' if rota.feriado == 1  else 'Não') + '</td>' +                    
                        '<td>' + str(rota.trajeto.valor) + '</td>' +
                        '<td><a class="btn btn-success btn-xs" onclick="buscaTrajeto(' + "'" + str(rota.id) + "','" + rota.saida + "'" + ')">Visualizar</a></td>' +
                        '</tr>')

    retorno.append(html)
    retorno = json.dumps(retorno)
    return HttpResponse(retorno, content_type='application/json')

def getBairroByRota(request, *args, **kwargs):

    html = []
    mapQuery = []

    if request.method == 'POST':
        mapQuery = json.loads(request.POST['client_response'])

    bairros = Bairro.objects.all().filter(cidade=int(mapQuery["idCidade"]))

    for bairro in bairros:
        html.append('<option value="' + str(bairro.id) + '">' + bairro.descricao + '</option>')

    return HttpResponse(json.dumps(html), content_type='application/json')

def getCidadeBairroByUserPosition(request, *args, **kwargs):
    cbCidade = []
    cbBairro = []
    mensagens = []
    retorno = []
    mapQuery = []
    cidadeSelecionada = 0
    bairroSelecionado = 0
    encontrouCidade = False
    encontrouBairro = False
    mensagem = ''

    if request.method == 'POST':
        mapQuery = json.loads(request.POST['client_response'])

    cidades = Cidade.objects.all()

    for cidade in cidades:
        html = '<option value="' + str(cidade.id) + '">' + cidade.descricao + '</option>'
        cbCidade.append(html)

        if cidade.descricao == mapQuery["nmCidade"].upper():
            cidadeSelecionada = cidade.id
            encontrouCidade = True

            for bairro in Bairro.objects.all().filter(cidade=cidade.id):
                htmlBairro = '<option value="' + str(bairro.id) + '">' + bairro.descricao + '</option>'
                cbBairro.append(htmlBairro)

                if bairro.descricao == mapQuery["nmBairro"].upper():
                    bairroSelecionado = bairro.id
                    encontrouBairro = True

    retorno.append(encontrouBairro)
    retorno.append(cbCidade)
    retorno.append(cbBairro)
    retorno.append(cidadeSelecionada)
    retorno.append(bairroSelecionado)

    mensagem = 'Localização encontrada com sucesso!'
    if not encontrouCidade:
        mensagem = 'Sua cidade não consta na base de dados!'
    elif not encontrouBairro:
        mensagem = 'Seu bairro não consta na base de dados!'
    retorno.append(mensagem)

    return HttpResponse(json.dumps(retorno), content_type='application/json')


###################################################################################################
# ---------------------------------- MANUTENCAO DE CIDADES -------------------------------------- #
###################################################################################################
def syncCidade(nomeCidade):
    cidade = Cidade.objects.all().filter(descricao=nomeCidade.upper())

    if len(cidade) == 0 and nomeCidade != '':
        cidade = Cidade()
        cidade.descricao = nomeCidade.upper()
        cidade.save()
        return cidade
    return cidade[0]

###################################################################################################
# ---------------------------------- MANUTENCAO DE BAIRROS -------------------------------------- #
###################################################################################################
def syncBairro(cidade, nomeBairro):
    bairro = Bairro.objects.all().filter(cidade=cidade.id, descricao=nomeBairro.upper())

    print('canaleto bairro: ' + str(len(bairro)))
    print('nomeBairro ->', nomeBairro)

    if len(bairro) == 0 and nomeBairro != '':
        print('igual a zero')
        bairro = Bairro()
        bairro.cidade = cidade
        bairro.descricao = nomeBairro.upper()
        bairro.save()
        return bairro

    print('aaaaaaaa')
    return bairro[0]

###################################################################################################
# ------------------------ MANUTENCAO ASSOCIACAO CIDADExBAIRROxTRAJETO -------------------------- #
###################################################################################################
def syncCidadeBairroTrajeto(cidade, bairro, trajeto):
    assoc = CidadeBairroTrajeto.objects.all().filter(cidade=cidade.id, bairro=bairro.id, trajeto=trajeto.id)
    if len(assoc) == 0:
        assoc = CidadeBairroTrajeto()
        assoc.cidade = cidade
        assoc.bairro = bairro
        assoc.trajeto = trajeto
        assoc.save()


###################################################################################################
# ------------------------------- LOGICAS GENERICAS DAS MANUTENCOES ----------------------------- #
###################################################################################################
def deleteObjeto(objeto, mensagem):
    messages = []
    objeto.delete()
    success = True
    messages.append(mensagem + ' removido com sucesso!')
    return myReturnJsonResponse(messages, success)


###################################################################################################
# ----------------------------------- LOGICAS BASICAS DO CONTEXTO ------------------------------- #
###################################################################################################

def getCRUDProperties(option):
    if option == 'ADD' or option == 'MOD':
        return addAndModProperties(option)
    else:
        return viewAndRemoveProperties(option)

def paginationProperties(result):
    result['qtdPaginas'] = ceil(len(result['listaItens']) / 10)
    result['loopPaginas'] = range(1, ceil(len(result['listaItens']) / 10) +1)
    return result

def viewAndRemoveProperties(option):
    properties = {}
    properties['crudFieldsProperties'] = 'disabled'
    properties['saveButtonProperties'] = 'style="display:none;"'
    properties['returnButtonLabel'] = "Cancelar"
    if option == 'RMV':
        properties['CRUDTitle'] = 'Remover'
    else:
        properties['CRUDTitle'] = 'Detalhar'
        properties['returnButtonLabel'] = "Voltar"
        properties['removeButtonProperties'] = 'style="display:none;"'

    return properties

def addAndModProperties(option):
    properties = {}
    properties['removeButtonProperties'] = 'style="display:none;"'
    properties['returnButtonLabel'] = "Cancelar"

    if option == 'ADD':
        properties['CRUDTitle'] = 'Adicionar'
    else:
        properties['CRUDTitle'] = 'Modificar'

    return properties;

def returnActionsTableRow(manutUrl, itemId):
    return ('<td class="actions" width="200">' +
            '<a class="btn btn-success btn-xs" href="' + manutUrl + 'VIEW/' + str(itemId) + '">Visualizar</a>' +
            '<a class="btn btn-warning btn-xs" href="' + manutUrl + 'MOD/'  + str(itemId) + '">Editar</a>' +
            '<a class="btn btn-danger btn-xs"  href="' + manutUrl + 'RMV/'  + str(itemId) + '">Excluir</a>' +
            '</td>')

def myReturnJsonResponse(messages, success):
    result = {}
    result['messages'] = messages
    result['success'] = success
    return JsonResponse(result)

def returnBooleanValue(requestPostValue, value):
    if value in requestPostValue and requestPostValue[value] == 'on':
        return True
    return False

###################################################################################################
# ---------------------------------------- DEMAIS FUNCOES  -------------------------------------- #
###################################################################################################

def calcChegadaRota(trajeto):
    duracao = 0
    tempo = datetime.time()

    for itinerario in trajeto.getItinerario():
        duracao = duracao + (itinerario.tempo / 60)

    for rota in Rota.objects.all().filter(trajeto=trajeto.id):
        hora = rota.saida[0:rota.saida.index(':')]
        minutos = rota.saida[rota.saida.index(':') + 1:len(rota.saida)]
        minutos = Decimal(minutos) + duracao
        novoTempo = datetime.time(int(hora), int(tempo.minute + duracao))
        rota.chegada = novoTempo.strftime('%H:%M')
        rota.save()

def getDiasOperacao(rota):
    diasOperacao = ''
    if rota.segunda:
        diasOperacao = diasOperacao + 'SEG,'
    if rota.terca:
        diasOperacao = diasOperacao + 'TER,'
    if rota.quarta:
        diasOperacao = diasOperacao + 'QUA,'
    if rota.quinta:
        diasOperacao = diasOperacao + 'QUI,'
    if rota.sexta:
        diasOperacao = diasOperacao + 'SEX,'
    if rota.sabado:
        diasOperacao = diasOperacao + 'SAB,'
    if rota.domingo:
        diasOperacao = diasOperacao + 'DOM,'

    if len(diasOperacao) > 0:
        diasOperacao = diasOperacao[:len(diasOperacao) - 1]
    return diasOperacao

###################################################################################################
# ------------------------------- METODOS ACESSADOS EXTERNAMENTE  ------------------------------- #
###################################################################################################
def getRotasCorrente(request):
    resultRotas = Rota.objects.all()

    html = []
    rotas = []

    for rota in resultRotas:
        jsonObject = '{'
        jsonObject += '"rota": "' + str(rota.id) + '"'
        jsonObject += ',"diasOperacao": "' + getDiasOperacao(rota) + '"'
        jsonObject += ',"estudante": "' + str(rota.estudante) + '"'
        jsonObject += ',"especiais": "' + str(rota.especiais) + '"'
        jsonObject += ',"saida": "' + str(rota.saida) + '"'
        jsonObject += ',"chegada": "' + str(rota.chegada) + '"' 
        jsonObject += ',"trajeto": "' + str(rota.trajeto.id) + '"' 
        jsonObject += ',"descricao": "' + rota.trajeto.descricao + '"' 
        jsonObject += '}'
        html.append(jsonObject)

    return HttpResponse(json.dumps(html), content_type='application/json')

def iniciarMapeamento(request,idRota,latitude,longitude):

    mapeamento = MapeamentoOnibus()
    
    try:
        mapeamento = MapeamentoOnibus.objects.get(rota=idRota)
    except MapeamentoOnibus.DoesNotExist:
        mapeamento = None

    if mapeamento == None:
        mapeamento = MapeamentoOnibus()
        mapeamento.rota = Rota.objects.get(id=idRota)

    mapeamento.latitude = latitude
    mapeamento.longitude = longitude
    mapeamento.save()

    html = []
    return HttpResponse(json.dumps(html), content_type='application/json')
