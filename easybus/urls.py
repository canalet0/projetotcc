from django.conf.urls import url

from . import views

'''url(r'^(.*)/buscaClientes$',views.buscaClientes,name='buscaClientes'),'''
urlpatterns = [
    url(r'^$', views.home),
    url(r'^teste/$', views.teste),
    url(r'^trajeto/$', views.listTrajeto, name='trajeto'),
    url(r'^trajeto/(?P<crudOption>\w+)/$', views.manutTrajeto, name='manutTrajeto'),
    url(r'^trajeto/(?P<crudOption>\w+)/(?P<ID>[0-9]{1,4})/$', views.manutTrajeto, name='manutTrajeto'),

    url(r'^rota/$', views.listRota, name='rota'),
    url(r'^rota/(?P<crudOption>\w+)/$', views.manutRota, name='manutRota'),
    url(r'^rota/(?P<crudOption>\w+)/(?P<ID>[0-9]{1,4})/$', views.manutRota, name='manutRota'),

    url(r'^buscaOnibus/$', views.buscaOnibus, name='buscaOnibus'),
    url(r'^(.*)/getTrajetoByRota/$', views.getTrajetoByRota, name='getTrajetoByRota'),
    url(r'^(.*)/getItinerarioByTrajeto/$', views.getItinerarioByTrajeto, name='getItinerarioByTrajeto'),    
    url(r'^(.*)/retornaRotasBuscaOnibus/$', views.retornaRotasBuscaOnibus, name='retornaRotasBuscaOnibus'),
    url(r'^(.*)/getBairroByRota/$', views.getBairroByRota, name='getBairroByRota'),
    url(r'^(.*)/getCidadeBairroByUserPosition/$', views.getCidadeBairroByUserPosition, name='getCidadeBairroByUserPosition'),

    url(r'^getRotasCorrente/$', views.getRotasCorrente, name='getRotasCorrente'),
    url(r'^iniciarMapeamento/(?P<idRota>[0-9]{1,4})/(?P<latitude>.+)/(?P<longitude>.+)/$', views.iniciarMapeamento, name='iniciarMapeamento'),
]