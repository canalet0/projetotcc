from __future__ import unicode_literals
import json

from django.db import models

class Trajeto(models.Model):
    descricao = models.CharField(max_length=255, blank=True, null=False, default="")
    valor = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    def getItinerario(self):
        return Itinerario.objects.all().filter(trajeto=self.id).order_by('sequencia')

class Itinerario(models.Model):
    trajeto = models.ForeignKey(Trajeto, on_delete=models.CASCADE)
    local = models.CharField(max_length=255, blank=True, null=True)
    bairro = models.CharField(max_length=255, blank=True, null=True)
    cidade = models.CharField(max_length=255, blank=True, null=True)
    sequencia = models.IntegerField(null=False)
    latitude = models.CharField(max_length=255, blank=True, null=True)
    longitude = models.CharField(max_length=255, blank=True, null=True)
    parada = models.BooleanField(default=False)
    tempo = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    tempoAdicional = models.DecimalField(max_digits=10, decimal_places=2, null=True)

class Rota(models.Model):
    trajeto = models.ForeignKey(Trajeto, on_delete=models.CASCADE)
    saida = models.CharField(max_length=255)
    chegada = models.CharField(max_length=255)
    estudante = models.BooleanField(default=False)
    segunda = models.BooleanField(default=False)
    terca = models.BooleanField(default=False)
    quarta = models.BooleanField(default=False)
    quinta = models.BooleanField(default=False)
    sexta = models.BooleanField(default=False)
    sabado = models.BooleanField(default=False)
    domingo = models.BooleanField(default=False)
    feriado = models.BooleanField(default=False)
    especiais = models.BooleanField(default=False)
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

class Cidade(models.Model):
    descricao = models.CharField(max_length=255)

class Bairro(models.Model):
    cidade = models.ForeignKey(Cidade, on_delete=models.CASCADE, blank=True, null=True)
    descricao = models.CharField(max_length=255)


class CidadeBairroTrajeto(models.Model):
    cidade = models.ForeignKey(Cidade, on_delete=models.CASCADE)
    bairro = models.ForeignKey(Bairro, on_delete=models.CASCADE)
    trajeto = models.ForeignKey(Trajeto, on_delete=models.CASCADE)

class MapeamentoOnibus(models.Model):
    rota = models.ForeignKey(Rota, on_delete=models.CASCADE)
    latitude = models.CharField(max_length=255, blank=True, null=True)
    longitude = models.CharField(max_length=255, blank=True, null=True)
