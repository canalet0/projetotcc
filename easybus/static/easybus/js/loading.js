var loadingVar;

function showLoading(){
	loadingVar = setTimeout(showLoadingEffect, 500);	
}

function showLoadingEffect(){
	$("#loaderBackground").show();
	$("#loader").show();
}

function hideLoading(){
	clearTimeout(loadingVar);
	$( "#loaderBackground").fadeOut(400, function(){
    	$( "#loader").fadeOut();
    });
}