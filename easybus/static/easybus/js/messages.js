function mountMessage(title,message){
	var notyMessage = "";
	if(title != '')
		notyMessage = '<strong>' + title + '</strong> </br>' + message;
	 
	return notyMessage;
}

function dispWarning(title,message){
	var notyMessage = mountMessage(title == '' ? 'Atenção!!!' : title,message);
	
	dispMessage(notyMessage,'warning');
}

function dispSuccess(title,message){
	var notyMessage = mountMessage(title == '' ? 'Sucesso!!!' : title,message);
	
	dispMessage(notyMessage,'success');
}

function dispError(title,message){
	var notyMessage = mountMessage(title == '' ? 'Erro!' : title,message);
	
	dispMessage(notyMessage,'error');
}

function dispInfo(title,message){
	var notyMessage = mountMessage(title == '' ? 'Informação!' : title,message);
	
	dispMessage(notyMessage,'information');
}

function dispMessage(message,type){
	noty({
		layout: 'topRight',
		theme: 'relax',
		type: type,
		text: message,
		timeout: 5000,
		closeWith: ['click'],
		animation: {
	        open: 'animated fadeIn', // Animate.css class names
	        close: 'animated fadeOut', // Animate.css class names
	    }
	});
}