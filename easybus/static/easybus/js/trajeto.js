var itinerarios = [];
var crudOption = "";
var action = "";
var selectedItinerarioPos;
var hasSequenceModified = false;
var hasAddressModified = false;
var newItem; 
var hasCompleteLoading = false;
var itinerarioPosForAjustaTempo = 0;

function adicionaItinerario(){

	var tempoAdicional = $("#tempoAdicional").val();
	console.log(tempoAdicional);
	console.log(tempoAdicional.split(':')[0]);

	newItem = {
		sequencia:parseInt($("#sequencia").val()),
		parada:$('#parada').is(":checked"),
		local:$("#endereco").val(),
		rua:$("#endereco").val(),
		bairro:$("#bairro").val(),
		cidade:$("#cidade").val(),
		latitude:$("#latitude").val(),
		longitude:$("#longitude").val(),
		tempoAdicional: tempoAdicional.split(':')[0] * 3600 + tempoAdicional.split(':')[1] * 60,
		tempo:0, 
	};

	if(itinerarios.length > 0
	&& action != 'MOD'){
		calcularTempo(newItem,newItem.sequencia - 2,resultCalculoTempo); 
	}else{
		adicionaItinerarioLista();
	}
}

function adicionaItinerarioLista(){

	hasSequenceModified = false;
	hasAddressModified  = false;

	ajustaSequencia(newItem.sequencia);

	if(action == 'MOD'){
		if(itinerarios[selectedItinerarioPos].latitude  != newItem.latitude
		|| itinerarios[selectedItinerarioPos].longitude != newItem.longitude)
			hasAddressModified = true;
		else newItem.tempo = itinerarios[selectedItinerarioPos].tempo;

		itinerarios[selectedItinerarioPos] = newItem;
	}else itinerarios.push(newItem);

	organizaItinerario();

	action = "";

	if(hasAddressModified
	|| hasSequenceModified){
		ajustaTempo(0);
	}else{
		refreshItinerarios();
	}
}

function ajustaSequencia(sequenciaPar){

	if(action == 'MOD'
	&& selectedItinerarioPos == newItem.sequencia - 1)
		return;

	for(var i = 0; i < itinerarios.length;i++){
		if(itinerarios[i].sequencia == sequenciaPar){
			hasSequenceModified = true;
			ajustaSequencia(itinerarios[i].sequencia + 1);

			if(action == 'MOD'
			&& sequenciaPar == newItem.sequencia){
				if(itinerarios[selectedItinerarioPos].sequencia < sequenciaPar)
					itinerarios[i].sequencia = sequenciaPar - 1;
				else itinerarios[i].sequencia = sequenciaPar + 1;
			}else itinerarios[i].sequencia = sequenciaPar + 1;
		}		
	}
}

function ajustaTempo(position){
	itinerarioPosForAjustaTempo = position + 1;
	calcularTempo(itinerarios[itinerarioPosForAjustaTempo],itinerarioPosForAjustaTempo - 1 ,setaTempo); 
}

function refreshItinerarios(){
	manejarItinerarios();
	adicionaItensHtml();
}

function addItinerarioObject(object){
	console.log("addItinerarioObject: " + JSON.stringify(object));
	itinerarios.push(object);
}

function manejarItinerarios(){
	$("#listTrajeto tbody").empty();

	var tableRows = "";

	for(var i = 0; i < itinerarios.length;i++){
		tableRows = tableRows + '<tr>' +
			                        '<td>' + itinerarios[i]["sequencia"]   + '</td>' +
			                        '<td>' + (itinerarios[i]["parada"] ? 'Sim' : 'Não')   + '</td>' +
			                        '<td>' + itinerarios[i]["local"]   + '</td>' +
															'<td>' + itinerarios[i]["bairro"] + '</td>' +
			                        '<td>' + itinerarios[i]["cidade"] + '</td>' +
									'<td>' + secondsToMinutesText(itinerarios[i]["tempo"],false)  + '</td>' + 
									'<td>' + secondsToMinutesText(itinerarios[i]["tempoAdicional"])  + '</td>' + 
									'<td class="actions" width="220">'         +
				                        '<a class="btn btn-success btn-xs"  onclick="visualizaItem(' + i + ')"   >Visualizar</a>';
		
		if(crudOption != 'VIEW'){
			tableRows = tableRows + '<a class="btn btn-warning btn-xs"  onclick="modificaItem(' + i + ')"   >Modificar</a>' +
				                    '<a class="btn btn-danger btn-xs"  onclick="removeItem(' + i + ')"   >Excluir</a>';
		}

		tableRows = tableRows + '</td></tr>)';
    }

    $('#listTrajeto tbody').append(tableRows);
}

function removeItem(itemPos){
	itinerarios.splice( itemPos, 1 );
	organizaItinerario();

	if(itinerarios.length > itemPos){
		newItem = itinerarios[itemPos - 1];
		calcularTempo(itinerarios[itemPos],itemPos - 1,atualizaCalculoTempoSequenciaModificada);
	}
}

function clearModal(){
	if(action != "")
		return;

	$('#buscaEndereco').val("");
	$('#sequencia').val("");
	$('#bairro').val("");
	$('#cidade').val("");
	$('#parada').prop('checked', false);
	$('#endereco').val("");
	$('#latitude').val("");
	$('#longitude').val("");
	$('#tempoAdicional').val("00:00");
}

function adicionaItensHtml(){
	var itinerarioJson = "";
	for(var i = 0; i < itinerarios.length;i++){
		itinerarioJson = itinerarioJson + 
		"<input type='hidden' class='form-control' id='itinerario[" + i + "]' name='itinerario' value='" + JSON.stringify(itinerarios[i]) + "'>";
	}
	$("#itinerarioSelecionado").empty();
	$("#itinerarioSelecionado").append(itinerarioJson);
}

function visualizaItem(itemPos){
	action = 'VIEW';
	selectedItinerarioPos = itemPos;
	visualizaDadosItem(itemPos,true);

}

function enableDisableFields(option){
	$('#sequencia').prop('disabled',option);
	$('#parada').prop('disabled',option);
	$('#latitude').prop('disabled',option);
	$('#longitude').prop('disabled',option);
	$('#tempo').prop('disabled',option);
}

function modificaItem(itemPos){
	action = 'MOD';
	selectedItinerarioPos = itemPos;
	visualizaDadosItem(itemPos,true);
}

function visualizaDadosItem(itemPos,habilitaCampos){

	abrirModal();

	var itinerario = itinerarios[itemPos];

	var horaAdicional = Math.floor(itinerario.tempoAdicional / 3600);
	var minutoAdicional = itinerario.tempoAdicional / 60 - (horaAdicional / 60);

	if (horaAdicional.toString().length < 2)
		horaAdicional = '0' + horaAdicional.toString();

	if (minutoAdicional.toString().length < 2)
		minutoAdicional = '0' + minutoAdicional.toString();

	var tempoAdicional = horaAdicional.toString() + ":" + minutoAdicional.toString();

	console.log(horaAdicional);
	console.log(minutoAdicional);
	console.log(tempoAdicional);

	$('#sequencia').val(itinerario.sequencia);
	$('#bairro').val(itinerario.bairro);
	$('#cidade').val(itinerario.cidade);
	$('#parada').prop('checked', itinerario.parada);
	$('#endereco').val(itinerario.local);	
	$('#latitude').val(itinerario.latitude);
	$('#longitude').val(itinerario.longitude);
	$('#tempoAdicional').val(tempoAdicional);

	if(hasCompleteLoading
	&& (   action == 'MOD'
	    || action == 'VIEW')){
		setMapPosition(new google.maps.LatLng(itinerarios[selectedItinerarioPos].latitude, itinerarios[selectedItinerarioPos].longitude));
	}
}

function abrirModal(){

	$('#mapModal').modal('show');
	$('#sequencia').val(itinerarios.length + 1);
	$("#divBuscaEndereco").show();

	enableDisableFields(action != 'VIEW' ? false : true);

	if(action == 'VIEW'){
		$("#divBuscaEndereco").hide();
	}

	if(action == '')
		action = 'ADD';
}

function completeInitializeTrajetoMapModal(){
	hideLoading();
	enableDisableFields(action != 'VIEW' ? false : true);
	hasCompleteLoading = true;
	if(action == 'MOD'
	|| action == 'VIEW'){
		setMapPosition(new google.maps.LatLng(itinerarios[selectedItinerarioPos].latitude, itinerarios[selectedItinerarioPos].longitude));
	}
}

function fechaModal(){
	action = '';
}

function organizaItinerario(){
	var i;

	itinerarios.sort(comparaSequencia); 

	for(i = 0; i < itinerarios.length;i++){
		itinerarios[i].sequencia = i + 1;
	}

	refreshItinerarios();
}

function comparaSequencia(a,b) {
	if (a.sequencia < b.sequencia)
		return -1;
	
	if (a.sequencia > b.sequencia)
		return 1;
	
	return 0;
}

function calcularTempo(destino,origemPosition,callback){

	if(itinerarios.length <= origemPosition)
		return;

	var origem = itinerarios[origemPosition];

	
	if(typeof origem  === "undefined"
	|| typeof destino === "undefined"){
		callback(0);
	}else{
		var from = new google.maps.LatLng(origem.latitude,origem.longitude);
		var to = new google.maps.LatLng(destino.latitude,destino.longitude);

		calcTime(from,to,callback); 
	}
} 

function atualizaCalculoTempoSequenciaModificada(result){
	itinerarios[newItem.sequencia].tempo = result;
	refreshItinerarios();
}

function atualizaTempoSequenciaAntiga(result){
	itinerarios[selectedItinerarioPos + 1].tempo = result;
	refreshItinerarios();
}
 
function resultCalculoTempo(result){ 
	newItem.tempo = result; 
	adicionaItinerarioLista(); 
} 

function setaTempo(result){
	itinerarios[itinerarioPosForAjustaTempo].tempo = result;

	if(itinerarioPosForAjustaTempo < itinerarios.length - 1){
		ajustaTempo(itinerarioPosForAjustaTempo);
	}else{
		refreshItinerarios();
	}
}

function enderecoAlterado(){

	var endereco = addressObject.rua;

	if(addressObject.numero != undefined
	&& addressObject.numero != "")
		endereco = endereco + ", " + addressObject.numero;

	$("#endereco").val(endereco);
	$("#bairro").val(addressObject.bairro);
	$("#cidade").val(addressObject.cidade);

	if(mapLocation){
		$("#latitude").val(mapLocation.lat());
		$("#longitude").val(mapLocation.lng());
	}
}