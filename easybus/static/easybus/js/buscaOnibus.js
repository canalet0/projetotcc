var itinerarios = [];
var horarioSaida;
var posicaoOnibus;

function localizacaoAtual(){
	// verifica se o navegador tem suporte a geolocalização
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position){ // callback de sucesso
			// ajusta a posição do marker para a localização do usuário			
			userPosition = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);

			geocoder = new google.maps.Geocoder();

			geocoder.geocode({ 'latLng': latlng }, function (results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					getCidadeBairroByUserPosition(results[0]);
				}
			});
		}, 
		function(error){ // callback de erro
			console.log(error);
			dispError('','Erro ao obter localização!');
		});
	} else {
		dispError('','Navegador não suporta Geolocalização!');
	}
}

function buscaBairrosCidade(selecao){

	var idCidade = $('#cidade' + selecao).val();

	var json = "{"
	json += '"idCidade":"' + idCidade.toString() + '"'; 
	json += "}"; 

	$.ajax({
		url : "getBairroByRota/",
		type : "POST",
		dataType : "json",
		data : {
            client_response : json,
            csrfmiddlewaretoken : $("[name='csrfmiddlewaretoken']").val()
		},
		success : function(retorno) {
			$('#bairro' + selecao).empty();
			if(retorno != ""){
				for(var i = 0; i < retorno.length; i++){
					$('#bairro' + selecao).append(retorno[i]);	
				}
			}
		},
		error : function(xhr,errmsg,err) {
			alert(xhr.status + ": " + xhr.responseText);
		}
	});
}

function getCidadeBairroByUserPosition(address){

	var addressComponents = address.address_components;
	var cidade = '';
	var bairro = '';

	for(var i = 0; i < addressComponents.length; i++){
		for(var j = 0; j < addressComponents[i].types.length;j++){
			if(addressComponents[i].types[j] == "sublocality")
				bairro = addressComponents[i].long_name;
			if(addressComponents[i].types[j] == "locality")
				cidade = addressComponents[i].long_name;
		}
	}

	var json = "{"
	json += '"nmCidade":"' + cidade + '"'; 
	json += ',"nmBairro":"' + bairro + '"'; 
	json += "}";

	$.ajax({
		url : "getCidadeBairroByUserPosition/",
		type : "POST",
		dataType : "json",
		data : {
            client_response : json,
            csrfmiddlewaretoken : $("[name='csrfmiddlewaretoken']").val()
		},
		success : function(retorno) {
			
			if(retorno[0]){
				$('#bairroOrigem').empty();
				$('#cidadeOrigem').empty();
				$('#cidadeOrigem').append(retorno[1]);
				$('#bairroOrigem').append(retorno[2]);
				$('#cidadeOrigem').val(retorno[3]);
				$('#bairroOrigem').val(retorno[4]);
				dispSuccess('',retorno[5]);
			}else{
				dispError('',retorno[5]);
			}
		},
		error : function(xhr,errmsg,err) {
			alert(xhr.status + ": " + xhr.responseText);
		}
	});
}

function retornaRotas(){

    var json = "{"
	json += '"cidadeOrigem":"' + $('#cidadeOrigem').val() + '"'; 
	json += ',"bairroOrigem":"' + $('#bairroOrigem').val() + '"';
	json += ',"cidadeDestino":"' + $('#cidadeDestino').val() + '"';
	json += ',"bairroDestino":"' + $('#bairroDestino').val() + '"';
	json += "}";
	
	$.ajax({
		url : "retornaRotasBuscaOnibus/",
		type : "POST",
		dataType : "json",
		data : {
            client_response : json,
            csrfmiddlewaretoken : $("[name='csrfmiddlewaretoken']").val()
		},
		success : function(retorno) {
			$('#manutListTable tbody').empty();
			if(retorno != ""){
				for(var i = 0; i < retorno.length; i++){
					$('#manutListTable tbody').append(retorno[i]);	
				}
			}
			console.log(retorno);

		},
		error : function(xhr,errmsg,err) {
			alert(xhr.status + ": " + xhr.responseText);
		}
	});
}

function buscaTrajeto(idRota,horarioSaidaPar){
	var json = "{"
	json += '"idRota":"' + idRota + '"'; 
	json += "}";

	horarioSaida = horarioSaidaPar;
	
	/*limpa os itinerarios*/
	itinerarios = [];

	$.ajax({
		url : "getTrajetoByRota/",
		type : "POST",
		dataType : "json",
		data : {
            client_response : json,
            csrfmiddlewaretoken : $("[name='csrfmiddlewaretoken']").val()
		},
		success : function(retorno) {

			deserializeDjangoObject(retorno['itinerario'],addItinerarioObject,setHorario);

			document.getElementById("mapa").addEventListener('fullLoadDirectionsMarker',function(){
				getUserLocation(mostraUsuarioMapa);
			});

			posicaoOnibus = JSON.parse(retorno['posicaoOnibus']);
			
		},
		error : function(xhr,errmsg,err) {
			alert(xhr.status + ": " + xhr.responseText);
		}
	});
}

function mostraUsuarioMapa(){


	var marker = new google.maps.Marker({
		position: userPosition,
		map: map,
		title:'Você está aqui!',
		icon: '/static/easybus/images/maps/userlocation.png'
	});

	var busPosition = new google.maps.LatLng(posicaoOnibus['latitude'], posicaoOnibus['longitude']);

	var busMarker = new google.maps.Marker({
		position: busPosition,
		map: map,
		title:'Aqui está o ônibus!',
		icon: '/static/easybus/images/maps/bus.png'
	});

	mostraParadaMaisProxima();
}

function setHorario(){
	for(var i = 0; i < itinerarios.length; i++){	
		var tempo = new Date();
		var duracao = 0;

		tempo.setHours(horarioSaida.substring(0,horarioSaida.indexOf(":")));
		tempo.setMinutes(horarioSaida.substring(horarioSaida.indexOf(":") + 1,horarioSaida.length));

		duracao = (parseFloat(itinerarios[i].tempo) / 60);

		tempo.setMinutes(tempo.getMinutes() + duracao);

		itinerarios[i].horario = (addLeftZeros(tempo.getHours(),2) + ":" + addLeftZeros(tempo.getMinutes(),2));
	}

	mostraTrajeto();
}

function mostraParadaMaisProxima(){
	var paradaMaisProxima = 0;
	var paradaMaisProximaValor = getDistanceFromUser(0);

	for (i = 1; i < myWaypoints.length; i++) {	
		distancia = getDistanceFromUser(i);
		if(distancia < paradaMaisProximaValor){
			paradaMaisProxima = i;
			paradaMaisProximaValor = distancia;
		}
	}
	
	mostraRotaParadaMaisProxima(paradaMaisProxima);
}

function getDistanceFromUser(pos){
	directionsService = new google.maps.DirectionsService();

	var request = { // Novo objeto google.maps.DirectionsRequest, contendo:
		origin: userPosition, // origem
		destination: new google.maps.LatLng(myWaypoints[pos].latitude, myWaypoints[pos].longitude), // destino
		travelMode: google.maps.TravelMode.DRIVING // meio de transporte, nesse caso, de carro
   	};

	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) { // Se deu tudo certo
			console.log(result.routes[0].legs[0].distance.value);
			return result.routes[0].legs[0].distance.value;
		}
	});
}

function mostraRotaParadaMaisProxima(position){

	var userDirectionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: true,
	  polylineOptions: {
      	strokeColor: "purple"
    	}
    });

	var userDirectionsService = new google.maps.DirectionsService();

	userDirectionsDisplay.setMap(map);
	$('#routeDescription').empty();
	userDirectionsDisplay.setPanel(document.getElementById("routeDescription"));

    var request = {
		origin: userPosition,
		destination: new google.maps.LatLng(myWaypoints[position].latitude, myWaypoints[position].longitude),
		travelMode: google.maps.TravelMode.DRIVING
	};

    userDirectionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			userDirectionsDisplay.setDirections(result);
		}
	});

	google.maps.event.trigger(map, 'resize');
}

function abreModalDestino(){
	$('#destinyModal').modal('show');
}