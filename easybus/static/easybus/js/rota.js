var wasMapCreated = false;

function mostraItinerarios(){
	$("#listTrajeto tbody").empty();

	var tableRows = "";

	for(var i = 0; i < itinerarios.length;i++){
		if($("#saida").val() != "")
			itinerarios[i].horario = calculaHorario(i);

		tableRows = tableRows + '<tr>' +
			                        '<td>' + itinerarios[i]["sequencia"]   + '</td>' +
			                        '<td>' + (itinerarios[i]["parada"] ? 'Sim' : 'Não')   + '</td>' +
			                        '<td>' + itinerarios[i]["local"]   + '</td>' +
									'<td>' + itinerarios[i]["bairro"] + '</td>' +
			                        '<td>' + itinerarios[i]["cidade"] + '</td>';

		if(itinerarios[i].horario)
			tableRows = tableRows +	'<td>' + itinerarios[i].horario + '</td>';
		else tableRows = tableRows +'<td> - </td>';
        tableRows = tableRows + '</tr>)';
    }

    $('#listTrajeto tbody').append(tableRows);
}

function mostraTrajeto(){	
	if(!wasMapCreated){
		$.when(setMyWaypoints(itinerarios)).then($('#mapModal').modal('show'));
	}else{
		deleteMarkers();
		setDirections(itinerarios);
		addDirectionMarkers();
		$('#mapModal').modal('show');
	}

}

function calculaHorario(itemPosition){
	var duracao = 0;
	var tempo = new Date();
	var saida = $("#saida").val();
	
	tempo.setHours(saida.substring(0,saida.indexOf(":")));
	tempo.setMinutes(saida.substring(saida.indexOf(":") + 1,saida.length));

	for(var i = 0;  i < itemPosition + 1; i++){
		duracao = parseFloat(duracao) + (parseFloat(itinerarios[i].tempo) / 60);
		if(i > 0
		&& itinerarios[i - 1].tempoAdicional != null){

			duracao = duracao + (parseFloat(itinerarios[i - 1].tempoAdicional) / 60);
		}
	}

	tempo.setMinutes(tempo.getMinutes() + duracao);

	return (addLeftZeros(tempo.getHours(),2) + ":" + addLeftZeros(tempo.getMinutes(),2));
}

function calculaChegada(){
	var duracao = 0;
	var tempo = new Date();
	var saida = $("#saida").val();

	tempo.setHours(saida.substring(0,saida.indexOf(":")));
	tempo.setMinutes(saida.substring(saida.indexOf(":") + 1,saida.length));

	for(var i = 0;  i < itinerarios.length; i++){
		duracao = parseFloat(duracao) + (parseFloat(itinerarios[i].tempo) / 60) + (parseFloat(itinerarios[i].tempoAdicional) / 60);
	}

	tempo.setMinutes(tempo.getMinutes() + duracao);

	$("#chegada").val(addLeftZeros(tempo.getHours(),2) + ":" + addLeftZeros(tempo.getMinutes(),2));
	$("#chegadaAux").val($("#chegada").val());
	mostraItinerarios();
}

function getItinerario(){

	var json = "{"
	json += '"idTrajeto":"' + $("#cbTrajeto").val() + '"'; 
	json += "}";

	itinerarios = [];

	$.ajax({
		url : 'getItinerarioByTrajeto/',
		type : "POST",
		dataType : "json",
		data : {
            client_response : json,
            csrfmiddlewaretoken : $("[name='csrfmiddlewaretoken']").val()
		},
		success : function(retorno) {

			var success = retorno['success'];
			var messages = retorno['messages'];
			var retornoItinerario = retorno['itinerario'];
			
			if(success){
				deserializeDjangoObject(retornoItinerario,addItinerarioObject,mostraItinerarios);
				calculaChegada();
			}else{
				(function myLoop (i) {
					setTimeout(function () {   
						dispError('',messages[(i * -1) + messages.length]);
						if (--i) myLoop(i);
					},500)
				})(messages.length);
			}
		},
		error : function(xhr,errmsg,err) {
			dispError('','Erro com o servidor de aplicação! Entre em contato com o suporte.');
		}
	});
}