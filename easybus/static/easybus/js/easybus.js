var currentRecord = 1;
var currentPage = 1;
var qtdPaginas = 0;
var manutUrl = "";
var startPageForm;
var deleteItemCode = "";

function openDeleteModal(itemId){
	$('#deleteModal').modal('show');
	deleteItemCode = itemId;
}

function setStartPageForm(myform) {
	startPageForm = myform;
}

function atualizaQtdPaginas(qtd) {
	
	if(qtd <= 1){
		$("#btnNext").hide();
		$("#btnPrevious").hide();
		$("#page1").hide();
	}
	qtdPaginas = qtd;
	showPage(currentPage);
}

function showRecords(from,to){   
    var rows = document.getElementById('manutListTable').rows;
    for (var i = 1; i < rows.length; i++) {
        if (i < from || i > to)  
            rows[i].style.display = 'none';
        else
            rows[i].style.display = '';
    }
}

function showNextRecords(){   
	showPage(currentPage + 1);
}

function showPreviousRecords(){  
	showPage(currentPage - 1);
}

function showPage(page){
	
	if (page <  1)
		page = 1;
	else if (page > qtdPaginas)
		page = qtdPaginas;
	
	page == qtdPaginas ? $("#btnNext").attr('class', 'disabled') : $("#btnNext").attr('class', 'next');
		
	page == 1 ? $("#btnPrevious").attr('class', 'disabled') : $("#btnPrevious").attr('class', 'previous');
			
	setPageSelected(page);
	
	currentPage = page;
	currentRecord = page * 10;
	showRecords(currentRecord - 9,currentRecord);
}

function setPageSelected(page){
	for (var i = 1; i < qtdPaginas + 1; i++) {
		$("#page" + i).attr('class', '');
	}
	$("#page" + page).attr('class', 'active');
}

function clearData(){
	openAdvSearch();
	setSearchData();	
}

function searchData(searchParameters,searchUrl,type,objectId) {

	var json = "{"
	json += searchParameters;
	json += "}";
	
	$.ajax({
		url : searchUrl,
		type : "POST",
		dataType : "json",
		data : {
            client_response : json,
            csrfmiddlewaretoken : $("[name='csrfmiddlewaretoken']").val()
		},
		success : function(retorno) {
			$('#'+ objectId + ' tbody').remove();
			$('#' + type).empty();
			if(retorno != ""){
				if(type == "table" || type == "zoomTable"){
					$('#'+ objectId).append(retorno);
				}else{
					for(var i = 0; i < retorno.length; i++){
						$('#' + type).append(retorno[i]);
					}
					$("#" + type).change();
				}
			}else{
				dispWarning('','Nenhum registro encontrado para o filtro informado.');
			}
		},
		error : function(xhr,errmsg,err) {
			alert(xhr.status + ": " + xhr.responseText);
		}
	});
}

function validateSubmit(){
	
	var form = $('#manutForm');
	
	if ($("#manutForm").length == 0){
		form = startPageForm;
	}
	var resultUrl = window.location.href;
	var path = window.location.pathname;
	
	path = path.substring(0,path.indexOf('/',2) + 1);
	resultUrl = resultUrl.replace(window.location.pathname,'');
	
	$.ajax({
		url : window.location.pathname == '' ? window.location.href : manutUrl,
		type : "POST",
		data : form.serialize(),
		success : function(retorno) {
			var success = retorno['success'];
			var messages = retorno['messages'];
			
			if(success){
				window.sessionStorage.setItem('successMessages',JSON.stringify(messages));
				if(retorno['resultUrl']){
					resultUrl = retorno['resultUrl'];
				}else{ 
					resultUrl = resultUrl + path;
				}
				window.location.href = resultUrl;
			}else{
				(function myLoop (i) {
					setTimeout(function () {   
						dispError('',messages[(i * -1) + messages.length]);
						if (--i) myLoop(i);
					},500)
				})(messages.length);
			}
		},
		error : function(xhr,errmsg,err) {
			dispError('','Erro com o servidor de aplicação! Entre em contato com o suporte.');
		}
	});	
}

function deleteSubmit(){
	$.ajax({
		url : (window.location.pathname == '' ? window.location.href : manutUrl) + "RMV/" + deleteItemCode + "/",
		type : "POST",
		data: $('#deleteForm').serialize(),
		success : function(retorno) {
			var success = retorno['success'];
			var messages = retorno['messages'];
			
			if(success){
				window.sessionStorage.setItem('successMessages',JSON.stringify(messages));
				location.reload();
			}else{
				(function myLoop (i) {
					setTimeout(function () {   
						dispError('',messages[(i * -1) + messages.length]);
						if (--i) myLoop(i);
					},500)
				})(messages.length);
			}
		},
		error : function(xhr,errmsg,err) {
			dispError('','Erro com o servidor de aplicação! Entre em contato com o suporte.');
		}
	});
}

function deserializeDjangoObject(myObject, myfunction = null, callback = null){
	var objects = JSON.parse(myObject);

	console.log('canaleto qtd objetcs: ' + objects.length);
	
	$.each(objects, function (index, value) {
		if(myfunction){
			myfunction(value.fields);
		}
	});

	if(callback){
		callback();
	}
}

