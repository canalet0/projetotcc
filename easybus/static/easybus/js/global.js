function secondsToMinutesText(secondsParameter,retornaSegundos){ 
    var result; 
    var minutes = secondsToMinutes(secondsParameter); 
    var seconds; 
 
    result = minutes.toString() + " minuto"; 
    if(minutes > 1) 
        result = result + "s"; 
 
    if(retornaSegundos){ 
        seconds = secondsToMinutesWithSecods(secondsParameter) - minutes; 
        result = result + " " + seconds.toString() + " segundo"; 
        if(seconds > 1) 
            result = result + "s"; 
    } 
 
    return result; 
} 
 
function secondsToMinutes(secondsParameter){ 
    var division = secondsParameter / 60; 
    return Math.trunc(division); 
} 
 
function secondsToMinutesWithSecods(secondsParameter){     
    var division = secondsParameter / 60; 
    return division; 
}


function addLeftZeros(string,finalLength) {

    while (string.toString().length < finalLength) {
        string = "0" + string;
    }
    return string;
}

function addRightZeros(string,finalLength) {

    while (string.toString().length < finalLength) {
        string = string + "0";
    }
    return string;
}