var map;
var mapOptions = {
    zoom: 5,
    center: new google.maps.LatLng(-18.8800397, -47.05878999999999),
    mapTypeId: google.maps.MapTypeId.ROADMAP
};
var autocomplete;
var geocoder;
var marker;
var directions;
var directionsDisplay;
var directionsService;
var changePlaceFunction;
var addressObject = {
    name : '',
    numero : '',
    rua : '',
    bairro : '',
    cidade : '',
    estado : ''
}
var mapLocation;
var markers = [];
var myWaypoints = [];
var mostraSomenteParadas = false;
var userPosition;
var mapDivId;

function getUserLocation(callback){
    if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position){ // callback de sucesso
			// ajusta a posição do marker para a localização do usuário			
            console.log('posicao usuario lat: ' + position.coords.latitude + ' lng: ' + position.coords.longitude);
            console.log('posicao usuario lat: ' + position.coords.latitude + ' lng: ' + position.coords.longitude);
            console.log('posicao usuario lat: ' + position.coords.latitude + ' lng: ' + position.coords.longitude);
			userPosition = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
            callback();
        });
    }
}

function setChangedPlaceFunction(callbackFunction){
    changePlaceFunction = callbackFunction;
}

function createMap(divId,textInputId,callback){
    map = new google.maps.Map(document.getElementById(divId), mapOptions);
    mapDivId = divId;

    geocoder = new google.maps.Geocoder();

    initializeMarker(textInputId);

    if(textInputId)
        initializeAutoComplete(textInputId);

    if(callback)
        callback();

    if(myWaypoints.length > 0){
		setRoute(myWaypoints);
	}
}

function initializeAutoComplete(textInputId){
    var autoCompleteInput = document.getElementById(textInputId);

    autocomplete = new google.maps.places.Autocomplete(autoCompleteInput);

    google.maps.event.addListener(autocomplete, 'place_changed', setAutoCompleteLocation);
}

function initializeMarker(textInputId){
    marker = new google.maps.Marker({
        map: map,
        draggable: true,
    });

     google.maps.event.addListener(marker, 'drag', function () {
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                mountAddressObject(results[0]);

                mapLocation = marker.getPosition();

                if(textInputId){
                    $("#" + textInputId).val(addressObject.enderecoCompleto);                    
                }
            }
        });
    });
}

function setMyWaypoints(itinerario){
	myWaypoints = itinerario;
}

function setAutoCompleteLocation() {

    var location = autocomplete.getPlace();

    if(location
    && location.geometry
    && location.geometry.location){

        mapLocation = location.geometry.location;

        mountAddressObject(location);

        setMapPosition(mapLocation);
    }
}

function calcTime(from,to,callback){ 

	directionsService = new google.maps.DirectionsService();

	var request = { // Novo objeto google.maps.DirectionsRequest, contendo:
		origin: from, // origem
		destination: to, // destino
		travelMode: google.maps.TravelMode.DRIVING // meio de transporte, nesse caso, de carro
   	};
 
	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) { // Se deu tudo certo
			if(callback) 
        		callback(result.routes[0].legs[0].duration.value); 
    	}else{ 
      		if(callback) 
        		callback(0); 
    	} 
	});
}

function setDirections(routeDirections){
    directions = [];
    directions = routeDirections;
}

function setRoute(routeDirections){
    setDirections(routeDirections);

    directionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: true
    });

	directionsService = new google.maps.DirectionsService();

    var originLatLng = new google.maps.LatLng(directions[0].latitude, directions[0].longitude);
	var destinyLatLng = new google.maps.LatLng(directions[directions.length-1].latitude, directions[directions.length-1].longitude);

    directionsDisplay.setMap(map);
    if(!userPosition)
        directionsDisplay.setPanel(document.getElementById("routeDescription"));

    var request = {
		origin: originLatLng,
		destination: destinyLatLng,
		waypoints: addWayPoints(),
		travelMode: google.maps.TravelMode.DRIVING
	};

    directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(result);
		}
	});

    addDirectionMarkers();
}

function addWayPoints(){
	var waypts = [];

	if(directions.length > 2){
		for (i = 1; i < directions.length -1; i++) {
			waypts.push({
				location: latlng = new google.maps.LatLng(directions[i].latitude, directions[i].longitude),
				stopover: true
			});
		}
	}

	return waypts;
}

function addDirectionMarkers() {

    var icon = '';
    var sequenciaParada = 0;

    for (i = 0; i < directions.length; i++) {

        if(!directions[i].parada && mostraSomenteParadas)
            continue;

        icon = '';
        markerTitle = '';

        if(!directions[i].parada){
            icon = '/static/easybus/images/maps/desvio.png';
            markerTitle = 'Desvio';
        }else{
            sequenciaParada = sequenciaParada + 1;
            markerTitle = 'Parada: ' + sequenciaParada;
        }

        if(i == 0){
            icon = '/static/easybus/images/maps/inicio.png';
            markerTitle = 'Inicio do trajeto - ' + markerTitle;
        }else if(i == directions.length - 1){
            icon = '/static/easybus/images/maps/fim.png';
            markerTitle = 'Fim do trajeto - ' + markerTitle;
        }

        var horario = 'Não definido';

        if(directions[i].horario)
            horario = directions[i].horario;

    	var html = '<b>' + markerTitle + '</b> <br/> Horário: ' + horario +   '<br/>' + directions[i].local;

	    var marker = new google.maps.Marker({
	      position: latlng = new google.maps.LatLng(directions[i].latitude, directions[i].longitude),
	      map: map,
	      title:markerTitle,
	      zIndex: Math.round(latlng.lat() * -100000) << 5,
          icon: icon
	    });

	    marker.info = new google.maps.InfoWindow({
	    	content: html
	    });

	    google.maps.event.addListener(marker, 'click', function() {
	        var marker_map = this.getMap();
	        this.info.open(marker_map,this);
	    });

        markers.push(marker);
    }

    document.getElementById(mapDivId).dispatchEvent(new Event('fullLoadDirectionsMarker'));
}

function mountAddressObject(address){

    var addressComponents = address.address_components;
    var enderecoCompleto;

    console.log(address);
    console.log(address.name);

    clearAddressObject();
    
    addressObject.name = address.name;

    for(var i = 0; i < addressComponents.length; i++){
        for(var j = 0; j < addressComponents[i].types.length;j++){
            if(addressComponents[i].types[j] == "street_number")
                addressObject.numero = addressComponents[i].long_name;
            if(addressComponents[i].types[j] == "route")
                addressObject.rua = addressComponents[i].long_name;
            if(addressComponents[i].types[j] == "sublocality")
                addressObject.bairro = addressComponents[i].long_name;
            if(addressComponents[i].types[j] == "locality")
                addressObject.cidade = addressComponents[i].long_name;
            if(addressComponents[i].types[j] == "administrative_area_level_1")
                addressObject.estado = addressComponents[i].short_name;
        }
    }

    if(addressObject.rua){
        console.log(addressObject);
        enderecoCompleto = addressObject.rua;
        if(addressObject.name != undefined
        && addressObject.name != addressObject.rua){
            enderecoCompleto = addressObject.name + " - " + addressObject.rua;
        }
    }

    if(addressObject.numero)
        enderecoCompleto = enderecoCompleto + ", " + addressObject.numero;

    if(addressObject.bairro)   
        enderecoCompleto = enderecoCompleto + ", " + addressObject.bairro;

    if(addressObject.cidade)
        enderecoCompleto = enderecoCompleto + ", " + addressObject.cidade;

    if(addressObject.estado)
        enderecoCompleto = enderecoCompleto + " - " + addressObject.estado;

    addressObject.enderecoCompleto = enderecoCompleto;

    if(changePlaceFunction)
        changePlaceFunction();
}

function clearAddressObject(){
    addressObject.name = '';
    addressObject.numero = '';
    addressObject.rua = '';
    addressObject.bairro = '';
    addressObject.cidade = '';
    addressObject.estado = '';
}

function setMapPosition(mapLocation){
    map.setCenter(mapLocation);
    map.setZoom(17);

    if(marker)
        marker.setPosition(mapLocation);
}

function deleteMarkers(){
    markers = [];
}